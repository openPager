/* ser.c
   (C) 2004-5 Captain http://www.captain.at
   
   Sends 3 characters (ABC) via the serial port (/dev/ttyS0) and reads
   them back if they are returned from the PIC.
   
   Used for testing the PIC-MMC test-board
   http://www.captain.at/electronic-index.php

*/
#include <stdio.h>   /* Standard input/output definitions */
#include <string.h>  /* String function definitions */
#include <unistd.h>  /* UNIX standard function definitions */
#include <fcntl.h>   /* File control definitions */
#include <errno.h>   /* Error number definitions */
#include <termios.h> /* POSIX terminal control definitions */

#include <stdlib.h>
#include <time.h>

#include "captain/capser.h"

int fd;

int initport(int fd) {
  struct termios options;
  // Get the current options for the port...
  tcgetattr(fd, &options);
  // Set the baud rates to 19200...
  cfsetispeed(&options, B19200);
  cfsetospeed(&options, B19200);
  // Enable the receiver and set local mode...
  options.c_cflag |= (CLOCAL | CREAD);

  options.c_cflag &= ~PARENB;
  options.c_cflag &= ~CSTOPB;
  options.c_cflag &= ~CSIZE;
  options.c_cflag |= CS8;
  options.c_cc[VMIN]     = 6;

  // Set the new options for the port...
  tcsetattr(fd, TCSANOW, &options);
  return 1;
}

int compare(char* A,char* B)
{
  int i=0; 
  while(A[i] != '\0')
    {
      if(A[i]!=B[i])
	{
	  printf("incorrect beacon\n");
	  return 0;
	}
      i++;
    }
  return 1;
}

int main(int argc, char **argv) {

  srand((unsigned)time(NULL));

  char * message[6];
  message[0] = "Als Alfred den Huehnerstall betrat fand er seine oma mit einem Kuecken im Auge, tot auf dem Boden liegen, in einer Ecke knatterte das Motorrad\n";
  message[1] = "Der Portier war sichtlich erschrocken, als sich die junge Dame als verwandelter Kaese entpuppte, hatte er doch auf ein feuchtes Abenteuer gehofft\n";
  message[2] = "Irgendwann stellte Conni dann fest, dass ihre Peruecke verrutscht war und alle das umgedrehte Kreuz auf ihrem Hinterkopf betrachten konnten... das war in der Kirche\n";
  message[3] = "Ihn belustigte die Vorstellung seinen Chef in C-String zu sehen, war er doch ein nerdiger Programmierer\n";
  message[4] = "Die Ameisen hatten schon sein halbes Gesicht aufgegessen, als Charlie ihren Vater im Keller fand\n";
  message[5] = "Ham Sie was getrunken? Fragt mich der Polizist...\n";

  
  fd = open("/dev/ttyUSB0", O_RDWR |  O_NOCTTY | O_NDELAY);
  if (fd == -1) {
    perror("open_port: Unable to open /dev/ttyUSB0 - ");
    return 1;
  } else {
        fcntl(fd, F_SETFL, 0);
  }
  

  printf("baud=%d\n", getbaud(fd));
  initport(fd);  
  printf("baud=%d\n", getbaud(fd));

  char sResult[254];
  int nr;
  //  fcntl(fd), F_SETFL, FNDELAY); // don't block serial read
  while(1)
    {
      if (!readport(fd,sResult)) {
	printf("read failed\n");
	close(fd);
	return 1;
      }
      printf("readport=%s\n", sResult);
      if(compare(sResult, "beacon"))
	{

	  nr=rand()%6;	  
  	  if (!writeport(fd, message[nr])) {
	    printf("write failed\n");
	  }
	  printf("written:%s\n", message[nr]);  
	}
      
      //    usleep(500000);
    }
  
  close(fd);
  return 0;
}
