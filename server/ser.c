/* ser.c
   (C) 2004-5 Captain http://www.captain.at
   
   Sends 3 characters (ABC) via the serial port (/dev/ttyS0) and reads
   them back if they are returned from the PIC.
   
   Used for testing the PIC-MMC test-board
   http://www.captain.at/electronic-index.php

*/
#include "ser.h"

int fd;

int initport(int fd) {
  struct termios options;
  // Get the current options for the port...
  tcgetattr(fd, &options);
  // Set the baud rates to 19200...
  cfsetispeed(&options, B19200);
  cfsetospeed(&options, B19200);
  // Enable the receiver and set local mode...
  options.c_cflag |= (CLOCAL | CREAD);

  options.c_cflag &= ~PARENB;
  options.c_cflag &= ~CSTOPB;
  options.c_cflag &= ~CSIZE;
  options.c_cflag |= CS8;
  options.c_cc[VMIN]     = 6;

  // Set the new options for the port...
  tcsetattr(fd, TCSANOW, &options);
  return 1;
}

int compare(char* A,char* B)
{
  int i=0; 
  while(A[i] != '\0')
    {
      if(A[i]!=B[i])
	{
	  printf("incorrect beacon\n");
	  return 0;
	}
      i++;
    }
  return 1;
}

int main(int argc, char **argv) {

  
  fd = open("/dev/ttyUSB0", O_RDWR |  O_NOCTTY | O_NDELAY);
  if (fd == -1) {
    perror("open_port: Unable to open /dev/ttyUSB0 - ");
    return 1;
  } else {
        fcntl(fd, F_SETFL, 0);
  }
  

  printf("baud=%d\n", getbaud(fd));
  initport(fd);  
  printf("baud=%d\n", getbaud(fd));

  SENDPACK testpack;
  strcpy(testpack.uid, "xaoe");
  testpack.nid = 12345;
  strcpy(testpack.message, "testtestest");

  char sResult[254];
  int nr;
  //  fcntl(fd), F_SETFL, FNDELAY); // don't block serial read


  while(1)
    {
      /*
	if (!readport(fd,sResult)) {
	printf("read failed\n");
	close(fd);
	return 1;
	}
	printf("readport=%s\n", sResult);
	if(compare(sResult, "beacon"))
	{
      */  
      if (!writeport(fd, &testpack)) {
	printf("write failed\n");
      }

      printf("written:%s\n", testpack.message);  
      usleep(5000000);      
    }      



  
  close(fd);
  return 0;
}
