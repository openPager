#ifndef __SER
#define __SER

#include <stdio.h>   /* Standard input/output definitions */
#include <string.h>  /* String function definitions */
#include <unistd.h>  /* UNIX standard function definitions */
#include <fcntl.h>   /* File control definitions */
#include <errno.h>   /* Error number definitions */
#include <termios.h> /* POSIX terminal control definitions */

#include <stdlib.h>
#include <time.h>

typedef struct _SENDPACK{
  char uid[10];
  uint nid:16;
  char message[160];
//unsigned int flags;
}SENDPACK;

#include "captain/capser.h"

#endif
