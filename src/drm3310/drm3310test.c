//////////////////////////////////////////////////////////
// Title:		drm3310test.c							//
// Version:		v1.0									//
// Date:		12.08.2004								//
// Last Change:	15.08.2004								//
// Desc:		Nokia 3310 Lcd Ansteuerung				//
// Author:		Thomas Weger							//
// Compiler:	AVR-GCC: WinAVR-20040720				//
//////////////////////////////////////////////////////////

#include <avr/io.h>
#include <avr/pgmspace.h>
#include <inttypes.h>
#include "drm3310.h"

int main(void)
{



  DDRB |= (1<<PB5)|(1<<PB3);
  PORTB |= (1<<PB5)|(1<<PB3);

  DDRB |= (1<<PC0);
  PORTB |= (1<<PC0);//|(1<<PC1)|(1<<PC2)|(1<<PC3)|(1<<PC4);

  /*glcdn_Delay(1500000);
  PORTB &= ~(1<<PB5);
  glcdn_Delay(1500000);
  PORTB |= (1<<PB5);
  */
	glcdn_spiInit();
	glcdn_InitDisplay();

	while(1)
	{
		glcdn_ShowImage(glcdNokia);

		glcdn_Delay(1500000);
		glcdn_ClearLcd();
		glcdn_ShowImage(glcdTarja);

		  glcdn_Delay(1500000);
		glcdn_ClearLcd();
		glcdn_TestA();

		  glcdn_Delay(1500000);
		
		glcdn_ClearLcd();
		
	}
}
