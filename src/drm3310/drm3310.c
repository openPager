//////////////////////////////////////////////////////////
// Title:		drm3310.c								//
// Version:		v1.0									//
// Date:		12.08.2004								//
// Last Change:	15.08.2004								//
// Desc:		Nokia 3310 Lcd Ansteuerung				//
// Author:		Thomas Weger							//
// Compiler:	AVR-GCC: WinAVR-20040720				//
//														//
// History:		20040815:	Release	v1.0				//
//////////////////////////////////////////////////////////

#include <avr/io.h>
#include <avr/pgmspace.h>
#include <inttypes.h>
#include "drm3310.h"

static uint8_t glcdCache[GLCD_CACHE_SIZE];
static uint8_t scrollCache[GLCD_PIXELX];

void glcdn_spiInit(void)
{
	// Init Serial Ports
  //	GLCD_SER_PORT |= (1<< GLCD_SCLK);
	GLCD_SER_DDR |= (1<< GLCD_SCLK);
	GLCD_SER_DDR |= (1<< GLCD_MOSI);
	GLCD_SER_DDR |= (1<< GLCD_SS);
	// Init Hardware SPI
	//	SPCR = (1<<MSTR)|(1<<SPE)|(1<<SPR0);
	// Clear Status Register
	//	SPSR;
}

void glcdn_spiSendByte(uint8_t data)
{

  /*****************Hardware|SPI***************
	// Chip Enable
  GLCD_CTRL_PORT &= ~(1<<GLCD_CE_);
	// Send Byte
	SPDR = data;
	// Wait until transfer complete
    while ( (SPSR & 0x80) != 0x80 );
	// Chip disable
    GLCD_CTRL_PORT |= (1<<GLCD_CE_);*/

    /**************Software|SPI****************/
    GLCD_SER_PORT &= ~(1<<GLCD_SCLK);
    GLCD_CTRL_PORT &= ~(1<<GLCD_CE_);

    for(uint8_t i=0; i<8; i++)
      {
	GLCD_SER_PORT &= ~(1<<GLCD_MOSI);

	if(data & 0b10000000)
	  GLCD_SER_PORT |= (1<<GLCD_MOSI);

	GLCD_SER_PORT |= (1<<GLCD_SCLK);
	GLCD_SER_PORT &= ~(1<<GLCD_SCLK);

	data <<= 1;
      }

    GLCD_CTRL_PORT |= (1<<GLCD_CE_);
}

void glcdn_InitDisplay(void)
{
	// Init Control Ports
	GLCD_CTRL_DDR |= (1<< GLCD_DC);
	GLCD_CTRL_DDR |= (1<< GLCD_CE_);
	GLCD_CTRL_DDR |= (1<< GLCD_RES_);
	
	// Reset Lcd
	GLCD_CTRL_PORT &= ~(1<< GLCD_RES_);
	glcdn_Delay(5000);
	GLCD_CTRL_PORT |= (1<< GLCD_RES_);
	//glcdn_Delay(100);

	// Enable extended Functionset
	glcdn_WriteCommand(GLCD_FUNCTIONSETEXT);
	// Set Tempcoefficient
	glcdn_WriteCommand(GLCD_TEMPCOEF);
	// Set Bias
	glcdn_WriteCommand(GLCD_BIAS);
	// Set Contrast
	glcdn_Contrast(0x40);
	// Enable normal Functionset
	glcdn_WriteCommand(GLCD_FUNCTIONSET);
	// Set Display to normal Mode
	glcdn_WriteCommand(GLCD_DISPLAYNORMAL);
	// Clear Lcd
	glcdn_ClearLcd();
}


void glcdn_WriteData(uint8_t myData)
{
	// Enable Data Transfer
	GLCD_CTRL_PORT |= (1<<GLCD_DC);
	// Send Byte
	glcdn_spiSendByte(myData);
}

void glcdn_WriteCommand(uint8_t myData)
{
	// Enable Control Transfer
	GLCD_CTRL_PORT &= ~(1<< GLCD_DC);
	// Send Byte
	glcdn_spiSendByte(myData);
}

void glcdn_SetCharAddrXY(uint8_t myX, uint8_t myY)
{
	glcdn_WriteCommand(myX | GLCD_SETXADDR);
	glcdn_WriteCommand(myY | GLCD_SETYADDR);
}
void glcdn_Delay(long ld)
{
	long d;
	for(d=0;d<ld;d++)
	{
		asm("NOP");	
		asm("NOP");	
		asm("NOP");	
		asm("NOP");	
		asm("NOP");	
		asm("NOP");	
	}
}

void glcdn_ClearLcd(void)
{
	uint8_t col;
	uint16_t myIndex = 0;
	for(uint8_t page = 0; page < 6; page++)
	{
		for (col = 0; col < GLCD_PIXELX; col++)
			glcdCache[myIndex++] = 0x00;
	}
	glcdn_WriteCache(0,0,504);
}

void glcdn_SetInvLcd(uint8_t myInv)
{
	if(myInv == 1)
		glcdn_WriteCommand(GLCD_DISPLAYINVERT);
	if(myInv == 0)
		glcdn_WriteCommand(GLCD_DISPLAYNORMAL);
}

void glcdn_PutStrXLine(uint8_t *data, uint8_t x, uint8_t line)
{
	uint8_t myPage = (line-1);
	uint8_t myXAddr = (x | GLCD_SETXADDR);
	uint8_t myPageAddr = (myPage | GLCD_SETYADDR);
	uint8_t myCount = 0;

	glcdn_WriteCommand(myXAddr);
	glcdn_WriteCommand(myPageAddr);

	while (*data)
	{
		for(uint8_t i=0; i<5; i++)
		{
		  glcdCache[(GLCD_PIXELX*myPage)+x+myCount] = (pgm_read_byte(&Font5x7[((*data - 0x20) * 5) + i]));
			myCount++;
			if(i == 4)
			{
				glcdCache[(GLCD_PIXELX*myPage)+x+myCount] = 0x00;
				myCount++;
			}

		}
		data++;
	}
	glcdn_WriteCache(x,myPage,myCount);

}

void glcdn_ShowImage(uint8_t image[])
{
	uint16_t myIndex = 0;
	uint8_t col;
	for(uint8_t page = 0; page < 6; page++)
	{
		for (col = 0; col < GLCD_PIXELX; col++)
		  glcdCache[myIndex++] = pgm_read_byte(&image[(GLCD_PIXELX*page)+col]);	// Cache
	}
	glcdn_WriteCache(0,0,504);
}

void glcdn_SetPixel(uint8_t dpX, uint8_t dpY, uint8_t mode)
{
	// mode 0 = clear, mode 1 = draw
	uint8_t myPage = (dpY / 8);
	if(mode == 0)
		glcdCache[(GLCD_PIXELX*myPage)+dpX] &= -(1<<(dpY-(myPage*8)));
	if(mode == 1)
		glcdCache[(GLCD_PIXELX*myPage)+dpX] |= (1<<(dpY-(myPage*8)));

	glcdn_WriteCache(dpX,myPage,1);
}

void glcdn_WriteCache(uint8_t x, uint8_t page, uint16_t size)
{
	glcdn_WriteCommand(GLCD_SETXADDR | x);
	glcdn_WriteCommand(GLCD_SETYADDR | page);
	uint16_t myIndex = (page*GLCD_PIXELX) + x;
	for(uint16_t i = 0;i<size;i++)
	{
		glcdn_WriteData(glcdCache[myIndex + i]);
	}
}

void glcdn_Contrast ( uint8_t contrast )
{
	glcdn_WriteCommand(GLCD_FUNCTIONSETEXT);
	glcdn_WriteCommand(GLCD_SETVOP | contrast);
	glcdn_WriteCommand(GLCD_FUNCTIONSET);
}

void glcdn_TestA(void)
{
	glcdn_PutStrXLine("01234567890123", 0, 1);
	glcdn_PutStrXLine("2 LCD Test", 0, 2);
	glcdn_PutStrXLine("3 LCD Test", 0, 3);
	glcdn_PutStrXLine("4 LCD Test", 0, 4);
	glcdn_PutStrXLine("5 LCD Test", 0, 5);
	glcdn_PutStrXLine("6 LCD Test", 0, 6);
}

