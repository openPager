#include "main.h"

#define F_CPU 8000000

/*Menue Entries */
/********************************************************************/
/**/static const char menue_str[]       PROGMEM = "menue";       /**/
/**/static const char new_message_str[] PROGMEM = "new message";/**/
/*********************************************************************/
/**/static const char messages_str[]    PROGMEM = "messages";     /**/
/**/static const char book_str[]        PROGMEM = "book";        /**/
/**/static const char cathegories_str[] PROGMEM = "cathegories";/**/
/**/static const char settings_str[]    PROGMEM = "settings";  /**/
/*********************************************************************/
/**/static const char pager_str[]       PROGMEM = "pager";        /**/
/**/static const char display_str[]     PROGMEM = "display";     /**/
/**/static const char firmware_str[]    PROGMEM = "firmware";   /**/
/*********************************************************************/
/**/static const char name_str[]        PROGMEM = "name";         /**/
/**/static const char pager_id_str[]    PROGMEM = "pager ID";    /**/
/**/static const char memory_str[]      PROGMEM = "memory";     /**/
/**/static const char sleep_str[]       PROGMEM = "sleep";     /**/
/****************************************************************/

static const char light_str[]       PROGMEM = "light";
static const char contrast_str[]    PROGMEM = "contrast";

static const char version_str[]     PROGMEM = "version";
static const char update_str[]      PROGMEM = "update";

extern const MENUE_ENTRY PROGMEM start, main_menue, new_message, messages, book, cathegories, settings, pager, display, firmware, name, pager_id, memory, sleep, light, contrast, version, update;

//               bezeichner            | string         | prev       | next       | up          | down        | function
const MENUE_ENTRY PROGMEM start       = { menue_str      , &main_menue, &main_menue, &main_menue , &main_menue , menu_function    };

const MENUE_ENTRY PROGMEM main_menue  = { menue_str      , &main_menue, &messages  , NULL        , &book       , menu_function    };
const MENUE_ENTRY PROGMEM new_message = { new_message_str, &main_menue, NULL       , &main_menue , NULL        , menu_function    };

const MENUE_ENTRY PROGMEM messages    = { messages_str   , &main_menue, NULL       , NULL        , &book       , view_messages    };
const MENUE_ENTRY PROGMEM book        = { book_str       , &main_menue, NULL       , &messages   , &cathegories, view_booking     };
const MENUE_ENTRY PROGMEM cathegories = { cathegories_str, &main_menue, NULL       , &book       , &settings   , view_cathegories };
const MENUE_ENTRY PROGMEM settings    = { settings_str   , &main_menue, &pager     , &cathegories, NULL        , menu_function    };

const MENUE_ENTRY PROGMEM pager       = { pager_str      , &messages  , &name      , NULL        , &display    , menu_function    };
const MENUE_ENTRY PROGMEM display     = { display_str    , &messages  , &light     , &pager      , &firmware   , menu_function    };
const MENUE_ENTRY PROGMEM firmware    = { firmware_str   , &messages  , &version   , &display    , NULL        , menu_function    };

const MENUE_ENTRY PROGMEM name        = { name_str       , &pager     , NULL       , NULL        , &pager_id   , view_name        };
const MENUE_ENTRY PROGMEM pager_id    = { pager_id_str   , &pager     , NULL       , &name       , &memory     , view_pager_id    };
const MENUE_ENTRY PROGMEM memory      = { memory_str     , &pager     , NULL       , &pager_id   , &sleep      , view_memory      };
const MENUE_ENTRY PROGMEM sleep       = { sleep_str      , &pager     , NULL       , &memory      , NULL        , view_sleep       };

const MENUE_ENTRY PROGMEM light       = { light_str      , &pager     , NULL       , NULL        , &contrast   , view_light       };
const MENUE_ENTRY PROGMEM contrast    = { contrast_str   , &pager     , NULL       , &light      , NULL        , view_contrast    };

const MENUE_ENTRY PROGMEM version     = { version_str    , &pager     , NULL       , NULL        , &update     , view_version     };
const MENUE_ENTRY PROGMEM update      = { update_str     , &pager     , NULL       , &version    , NULL        , view_update      };


/*
const char cath0[] PROGMEM = "PUNK";
const char cath1[] PROGMEM = "ROCK";
const char cath2[] PROGMEM = "POP";
const char cath3[] PROGMEM = "TECHNO";
const char cath4[] PROGMEM = "HIPHOP";
const char cath5[] PROGMEM = "METAL";
const char cath6[] PROGMEM = "HARDCORE";
const char cath7[] PROGMEM = "THEATER";

const char *CATHEGORIES[] PROGMEM = {
  cath0,
  cath1,
  cath2,
  cath3,
  cath4,
  cath5,
  cath6,
  cath7,
};

//EEPROM
uint8_t eeCathSelect EEMEM = 0;//1Byte=8Bit fuer 8 Kathegorien, muss bei Boot in RAM geladen werden
*/

void flash_led(uint8_t);

int main(void)
{
  uart_init();
  uart_flushbuffer();
  //Timer setzen, PWM
  //Timer0 PinD6 OC0A
  TCCR0A = (1 << COM0A1) | (1 << COM0B1) | (1 << WGM01) | (1 << WGM00); // FAST PWM ON OC0A
  TCCR0B = (1 << CS00); // NO PRESCALER
  //TIMSK0 = (1 << OCIE0A);//interupt enable

  //Timer2
  TCCR2A = (1 << COM2A1) | (1 << WGM21) | (1 << WGM20); // FAST PWM ON OC0A
  TCCR2B = (1 << CS20); // NO PRESCALER
  //TIMSK0 = (1 << OCIE0A);//interupt enable

  //PINs konfiguriern
  DDRB  = (1 << DDB3) | (1 << DDB5);
  PORTB |= (1 << PB3);

  DDRD  = (1 << DDD6) | (1 << DDD5);
  PORTD |= (1 << PD6) | (1 << PD5);

  uint8_t i;
  char s[4];
  

  ADCSRA |= (1 << ADSC);            // start single conversion
  while (ADCSRA & (1 << ADSC));  // wait until conversion is done


  OCR0B=255;
  OCR0A=255;
  OCR2A=255;

  flash_led(5);

  menu_function(&start);

  return 0; 
}

void flash_led(uint8_t count)
{
  uint8_t i;
  
  for (i = 0; i < count; ++i) {
    PORTB |= (1 << PB5);
    _delay_ms(100);
    PORTB &= ~(1 << PB5);
    _delay_ms(100);
  }
}
