#ifndef __MAIN
#define __MAIN
#include <avr/io.h>
#include <util/delay.h>
#include <stdlib.h>
#include <inttypes.h>
#include <avr/pgmspace.h>
#include <avr/eeprom.h>
#include <avr/interrupt.h>

//PROGMEM
typedef struct _MENUE_ENTRY {
  const unsigned char *text;
  const struct _MENUE_ENTRY *prev;
  const struct _MENUE_ENTRY *next;
  const struct _MENUE_ENTRY *up;
  const struct _MENUE_ENTRY *down;
  void ( *fp )( struct _MENUE_ENTRY* );
}MENUE_ENTRY;

//PROGMEM
/*
typedef struct {
  const unsigned char *title;
}CATHEGORY;
*/

//EEPROM
typedef struct MESSAGES{
  const unsigned char *title;
  const unsigned char *text;
  const uint8_t cath;
  //  uint8_t bookable:1;//event title und id entspricht der message
  const char date[6];
  const char id[6];
  const struct MESSAGES *next;
}MESSAGE;

#include "menue.h"
#include "uart.h"
#include "drm3310/drm3310.h"

#endif
