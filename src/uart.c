#include "main.h"

void uart_init(void)
{
  //UART setzen

    UBRR0H = MYUBRR >> 8;
    UBRR0L = MYUBRR;
    UCSR0B = (1<<RXEN0)|(1<<TXEN0);
    //    UCSR0C = (1<<USBS0)|(3<<UCSZ00);
    UCSR0C = (1<<UCSZ01)|(1<<UCSZ00);

}

void uart_flushbuffer(void)
{
  unsigned char c;
  while (UCSR0A & (1 << RXC0))
    c = UDR0;
}

  /* Zeichen empfangen */
  uint8_t uart_getc(void)
  {
    
    while (!(UCSR0A & (1<<RXC0)))   // warten bis Zeichen verfuegbar
      {
      }
    return UDR0;                   // Zeichen aus UDR an Aufrufer zurueckgeben
  }
 
  void uart_gets( char* Buffer, uint8_t MaxLen )
  {
    uint8_t NextChar;
    uint8_t StringLen = 0;
 
    NextChar = uart_getc();         // Warte auf und empfange das n?chste Zeichen
 
    // Sammle solange Zeichen, bis:
    // * entweder das String Ende Zeichen kam
    // * oder das aufnehmende Array voll ist
    while( NextChar != '\n' && StringLen < MaxLen - 1 ) {
      *Buffer++ = NextChar;
      StringLen++;
      NextChar = uart_getc();
    }
 
    // Noch ein '\0' anh?ngen um einen Standard
    // C-String daraus zu machen
    *Buffer = '\0';
    uart_flushbuffer();
  }

int uart_putc(unsigned char c)
{
  while (!(UCSR0A & (1<<UDRE0)))
    {
    }                             
 
  UDR0 = c;

  return 0;
}
 
 
/* puts ist unabhaengig vom Controllertyp */
void uart_puts (char *s)
{
  while (*s)
    {   /* so lange *s != '\0' also ungleich dem "String-Endezeichen" */
      uart_putc(*s);
      s++;
    }
}
