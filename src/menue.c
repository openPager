#include "main.h"

extern const MENUE_ENTRY PROGMEM start, main_menue, new_message, messages, book, cathegories, settings, pager, display, firmware, name, pager_id, memory, sleep, light, contrast, version, update;

MESSAGE *first_msg;//Zeiger auf erste Funktion - Beginn von verketteter Liste

MENUE_ENTRY *actual_entry; 
uint8_t actual_row;


void menu_function( MENUE_ENTRY *parent )
{
  char lcd_value[15];
  uint8_t row=1;
  MENUE_ENTRY *entry; 

  entry = (MENUE_ENTRY*) pgm_read_word((*parent).next);
  actual_entry = entry;
  actual_row = 1;
  glcdn_SetInvLcd(1);//invertiert
  while(entry && row<6)
    {
      strcpy_P(lcd_value, (char *) pgm_read_word((*entry).text));
      glcdn_PutStrXLine(lcd_value, 0, row);
      entry =  (MENUE_ENTRY*) pgm_read_word((*entry).down);
      if(row==1)
	glcdn_SetInvLcd(0);
      row++;
    }
  return;
}

void scroll_up()
{
  char lcd_value[15];
  MENUE_ENTRY *prev;
  if(actual_row > 1)
    {
      prev = (MENUE_ENTRY*) pgm_read_word((*actual_entry).up);
      glcdn_SetInvLcd(1);//invertiert
      strcpy_P(lcd_value, (char *) pgm_read_word((*prev).text));
      glcdn_PutStrXLine(lcd_value, 0, actual_row);

      glcdn_SetInvLcd(0);//invertiert
      strcpy_P(lcd_value, (char *) pgm_read_word((*actual_entry).text));
      glcdn_PutStrXLine(lcd_value, 0, actual_row);

      actual_row--;
      actual_entry = prev;
    }
  return;
}

void scroll_down()
{
  char lcd_value[15];
  MENUE_ENTRY *next;
  if(actual_row < 6)
    {
      next = (MENUE_ENTRY*) pgm_read_word((*actual_entry).down);

      strcpy_P(lcd_value, (char *) pgm_read_word((*actual_entry).text));
      glcdn_PutStrXLine(lcd_value, 0, actual_row);

      glcdn_SetInvLcd(1);//invertiert
      strcpy_P(lcd_value, (char *) pgm_read_word((*next).text));
      glcdn_PutStrXLine(lcd_value, 0, actual_row);

      glcdn_SetInvLcd(0);//invertiert
      actual_row++;
      actual_entry = next;
    }
  return;
}

void enter()
{
  void (*fkt)(MENUE_ENTRY*) = (void*) pgm_read_word((*actual_entry).fp);
  fkt(actual_entry);
  return;
}

void back()
{
  menu_function((MENUE_ENTRY*) pgm_read_word((*actual_entry).prev));
  return;
}
