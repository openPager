#ifndef __MENUE
#define __MENUE
void view_name( MENUE_ENTRY* );
void view_pager_id( MENUE_ENTRY* );
void view_memory( MENUE_ENTRY* );
void view_sleep( MENUE_ENTRY* );
void view_light( MENUE_ENTRY* );
void view_contrast( MENUE_ENTRY* );
void view_version( MENUE_ENTRY* );
void view_update( MENUE_ENTRY* );

void read_message( MENUE_ENTRY* );

void view_messages( MENUE_ENTRY* );
void view_cathegories( MENUE_ENTRY* );
void view_booking( MENUE_ENTRY* );

void menu_function( MENUE_ENTRY* );
void scroll_up( void );
void scroll_down( void );
void enter( void );
void back( void );
#endif
