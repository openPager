// 
// Anpassungen im makefile:
//    ATMega8 => MCU=atmega8 im makefile einstellen
//    lcd-routines.c in SRC = ... Zeile anh?ngen
// 
#include <avr/io.h>
#include "lcd-routines.c"
#include "uart.c"
#include <inttypes.h>
#include <stdlib.h> 
#include <util/delay.h> 
#include <avr/interrupt.h> 

inline uint8_t debounce(volatile uint8_t *, uint8_t);
void generate_matrix(char *, char **);
void display(int, char **);
void init_malloc(char**, int,int);
void matrix_clear(char**);

char* rows[12];
int state;
int x, y;

int main(void)
{
  int col=0;
  x=0;
  y=0;
  state=0;

  uart_init();
  lcd_init();
  init_malloc(rows, 16, 12);

  sei();
  
  DDRB &= ~((1 << PB1)|(1 << PB2)|(1 << PB3));//(0 << DDB2)|(0 << DDB3)|(0 << DDB4);
  DDRB |= (1<<PB0);
  PORTB |= ( 1 << PB1 )|(1<<PB2)|(1<<PB3);                 /* Pullup-Widerstand aktivieren            */
  PORTB &= ~(1<<PB0);
  set_cursor(0,2);

  lcd_string("   Pager Demo");

    while(1)
    {

      if(debounce(&PINB, PB1))
	{
	  //	  uart_puts("\x1B[2J \x1B[0;0f");
	  uart_puts("beacon");
	  //	  uart_putc('\0');
	}

      if(debounce(&PINB, PB2)&&(col<7))
	display(++col, rows);

      if(debounce(&PINB, PB3)&&(col>0))
	display(--col, rows);

      if(state)
	{
	  col=0;
	  display(col, rows);
	  //	  uart_puts("\x1B[2J \x1B[0;0f");
	  PORTB |= (1<<PB0);
	  _delay_ms(500);
	  PORTB &= ~(1<<PB0);
	  state=0;
	}

    }
 
  return 0;
}

void init_malloc(char** rows, int x, int y)
{
  for(int i=0; i<y; i++ )
    rows[i]=malloc(x * sizeof(char));

}

void matrix_clear(char** rows)
{
  for(int i=0;i<12;i++)
    for(int j=0;j<16;j++)
      rows[i][j]=' ';
}

void display(int col, char** rows)
{
  lcd_clear();
  set_cursor(0,1);
  lcd_string(rows[col]);
  set_cursor(0,2);
  lcd_string(rows[col+1]);

}

/* Einfache Funktion zum Entprellen eines Tasters */
inline uint8_t debounce(volatile uint8_t *port, uint8_t pin)
{
  if ( ! (*port & (1 << pin)) )
    {
      /* Pin wurde auf Masse gezogen, 100ms warten   */
      _delay_ms(50);  // max. 262.1 ms / F_CPU in MHz
      _delay_ms(50); 
      if ( *port & (1 << pin) )
        {
	  /* Anwender Zeit zum Loslassen des Tasters geben */
	  _delay_ms(50);
	  _delay_ms(50); 
	  return 1;
        }
    }
  return 0;
}

ISR (USART_RXC_vect) {
  char buff;
buff=uart_getc();
  if(x==0&&y==0)
    matrix_clear(rows);

 rows[y][x++]=buff;

  if(buff=='\n')//13)
    {
      //      char text[4];

      //      uart_puts(itoa( (int)(rows[y][x-2]),text,10 ));
      rows[y][x-1]=' ';
      state=1;
      x=0;
      y=0;
    }
  if(x>=16)
    {
      x=0;
      y++;
    }
}
